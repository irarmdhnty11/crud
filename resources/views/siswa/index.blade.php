@extends('layouts.master')

@section('content')
			@if(session('sukses'))
			<div class="alert alert-dark" role="alert">
			  {{session('sukses')}}
			</div>
			@endif
			<div class="row">
				<div class="col-6">
					<h1>Data Siswa SMK 10</h1>
				</div>
				<div class="col-6">
					<button type="submit" class="btn btn-dark btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
					  <h5>Tambah Siswa</h5>
					</button>
				</div>
				<table class="table table-hover table-striped table-bordered">
					<thead class="thead-dark">
						<tr>
							<th>Nama Lengkap</th>
							<th>Nama Panggilan</th>
							<th>Kelas</th>
							<th>Jenis Kelamin</th>
							<th>Agama</th>
							<th>Alamat</th>
							<th></th>
						</tr>
					</thead>
@foreach($data_siswa as $siswa)
					<tr>
						<td>{{$siswa->nama_lengkap}}</td>
						<td>{{$siswa->nama_panggilan}}</td>
						<td>{{$siswa->kelas}}</td>
						<td>{{$siswa->jenis_kelamin}}</td>
						<td>{{$siswa->agama}}</td>
						<td>{{$siswa->alamat}}</td>
						<td>
							<a href="/siswa/{{$siswa->id}}/edit" class="btn btn-sm btn-secondary">Edit</a>
							<a href="/siswa/{{$siswa->id}}/delete" class="btn btn-sm btn-dark" onclick="return confirm('Apakah data akan didelete?')">Delete</a>
						</td>
					</tr> 
@endforeach
				</table>
			</div>
		
	
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	</body>
</html>

<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Data Siswa</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="/siswa/create" method="POST">
	       {{csrf_field()}}
			  <div class="form-group">
				    <label for="exampleInputEmail1">Nama Lengkap</label>
				    <input name="nama_lengkap" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama Lengkap" placeholder="Nama Lengkap">
			  </div>

			 <div class="form-group">
			     <label for="exampleInputEmail1">Nama Panggilan</label>
			     <input name="nama_panggilan" type="text" class="form-control" id="exampleInputEmail" aria-describedby="Nama Panggilan" placeholder="Nama Panggilan">
			  </div>

			   <div class="form-group">
			     <label for="exampleInputEmail1">Kelas</label>
			     <input name="kelas" type="text" class="form-control" id="exampleInputEmail" aria-describedby="kelas" placeholder="Kelas">
			  </div>

			 <div class="form-group">
		       <label for="exampleInputEmail1">Jenis Kelamin</label>
		       <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
		         <option>L</option>
		         <option>P</option>
		       </select>
		     </div>

		     <div class="form-group">
			    <label for="exampleInputEmail1">Agama</label>
				<input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Agama" placeholder="Agama">
			 </div>

		     <div class="form-group">
			    <label for="exampleFormControlTextarea1">Alamat</label>
			    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
		     </div>			
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-dark btn-sm">Input</button>
	    	</form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection