@extends('layouts.master')

@section('content')
			<h1>Edit Data Siswa</h1>
@if(session('sukses'))
			<div class="alert alert-dark" role="alert">
			  {{session('sukses')}}
			</div>
@endif
			<div class="row">
				<div class="col-lg-6">
				 <form action="/siswa/{{$siswa->id}}/update" method="POST" >
{{csrf_field()}}
					<div class="form-group">
						    <label for="exampleInputEmail1">Nama Lengkap</label>
						    <input name="nama_lengkap" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Nama Lengkap" placeholder="Nama Lengkap" value="{{$siswa->nama_lengkap}}">
					  </div>

					 <div class="form-group">
					     <label for="exampleInputEmail1">Nama Panggilan</label>
					     <input name="nama_panggilan" type="text" class="form-control" id="exampleInputEmail" aria-describedby="Nama Panggilan" placeholder="Nama Panggilan" value="{{$siswa->nama_panggilan}}">
					  </div>

					   <div class="form-group">
					     <label for="exampleInputEmail1">Kelas</label>
					     <input name="kelas" type="text" class="form-control" id="exampleInputEmail" aria-describedby="kelas" placeholder="Kelas" value="{{$siswa->kelas}}">
					  </div>

					 <div class="form-group">
				       <label for="exampleInputEmail1">Jenis Kelamin</label>
				       <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
				         <option value="L" @if($siswa->jenis_kelamin == 'L') selected @endif>L</option>
				         <option value="P" @if($siswa->jenis_kelamin == 'P') selected @endif>P</option>
				       </select>
				     </div>

				     <div class="form-group">
					    <label for="exampleInputEmail1">Agama</label>
						<input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Agama" placeholder="Agama" value="{{$siswa->agama}}">
					 </div>

				     <div class="form-group">
					    <label for="exampleFormControlTextarea1">Alamat</label>
					    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$siswa->alamat}}</textarea>
				     </div>	
				      <button type="submit" class="btn btn-dark btn-sm">Update</button>
	    		</form>		
	    		</div>
			</div>
@endsection	
