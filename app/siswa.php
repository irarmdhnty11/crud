<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['nama_lengkap','nama_panggilan','kelas','jenis_kelamin','agama','alamat'];
}
